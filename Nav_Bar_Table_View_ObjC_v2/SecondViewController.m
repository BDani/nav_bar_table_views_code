//
//  SecondViewController.m
//  Nav_Bar_Table_View_ObjC_v2
//
//  Created by Bruno Tavares on 20/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "SecondViewController.h"

NSUInteger const kLabelHeight = 300;
NSUInteger const kLabelWidth = 70;

@interface SecondViewController ()

@end

@implementation SecondViewController

@synthesize label;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.view addSubview:self.label];
    self.label.font = [UIFont boldSystemFontOfSize:90.0f];

    self.view.backgroundColor = [UIColor whiteColor];
}
@end
