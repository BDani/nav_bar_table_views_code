//
//  ViewController.m
//  Nav_Bar_Table_View_ObjC_v2
//
//  Created by Bruno Tavares on 20/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"

NSUInteger const kRowHeight = 60;

@interface ViewController () 

@property (nonatomic, strong) NSArray* tableData;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"View 1";
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.frame];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.rowHeight = kRowHeight;

    [self.view addSubview:tableView];

    self.tableData = @[@"Go To View 2", @"Go To View 3", @"Go To View 4", @"Go To View 5"];
    
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = self.tableData[indexPath.row];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SecondViewController *secondViewController = [[SecondViewController alloc] init];
    
    secondViewController.label = [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width / 2 - 50 / 2, [[UIScreen mainScreen] bounds].size.height / 2 - 200 / 2, 50, 200)];
        
    secondViewController.title = [NSString stringWithFormat:@"View %ld", (indexPath.row + 2)];
    
    
    [self.navigationController pushViewController:secondViewController animated:YES];
    
    secondViewController.label.text = [NSString stringWithFormat:@"%ld", (indexPath.row + 2)];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.tableData count];
}

@end
